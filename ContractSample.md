# Contract Sample

## How to add a clause?

1. Create a new entry in markdow at the end of the list
2. describe your clause in english and fill up the template
    1.  **Short Name**
        * **Category**: *category*
        * **Purpose**: *purpose*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: (to do|in progress|in review|done)
3. Commit
4. Make sure your clause appears with a section number

During the workdshop, groups will get assigned the clauses by IDs and resolve them. Once the result is shared, we will update the status and compile the result.

Many thanks for your contribution

## Contract clauses list

1. **Specific location restriction**
    * **Category**: *Location*
    * **Purpose**: *restrict the validity of a permission to a specific location*
    * **Full wording in is legal form**: Photographs are often licensed on a geographic basis: global rights will likely be more expensive than a country-specific permission. Such conditions are frequently modelled in ODRL by Constraints. The way of representing a geographic entity has not been standardized, though. Sample from https://w3c.github.io/odrl/bp/#how-to-restrict-the-validity-of-a-permission-to-a-specific-location
    * **status**: Done - [Odrl Directory - Specific-Location-Restriction](OdrlDirectory.md#1-specific-location-restriction)

2. **Limitation in the possible usage of data**
    * **Category**: *Data Usage*
    * **Purpose**: *No restriction*
    * **status**: In Review - [Odrl Directory - No-restriction in the possible usage of data for a specific purpose](OdrlDirectory.md#1-no-restriction-in-the-possible-usage-of-data-for-a-specific-purpose)

3. **Limitation in the possible usage of data**
    * **Category**: *Data Usage*
    * **Purpose**: *Only for a specific usage*
    * **status**: In Review - [Odrl Directory - Limitation in the possible usage of data for a specific purpose](OdrlDirectory.md#2-limitation-in-the-possible-usage-of-data-for-a-specific-purpose)
    

4. **Limitation in the possible usage of data**
    * **Category**: *Data Usage*
    * **Purpose**: *No restriction, except for a list of specific usages*
    * **status**: In Review - [Odrl Directory - Limitation in the possible usage of data for a list of specific purpose](OdrlDirectory.md#3-limitation-in-the-possible-usage-of-data-for-a-list-of-specific-purpose)

5. **Duration of data usage**
    * **Category**: *Duration*
    * **Purpose**: *No restriction*
    * **status**: In Review - [Limitation in the possible usage of data for a specific periode of time](OdrlDirectory.md#2-limitation-in-the-possible-usage-of-data-for-a-specific-periode-of-time-elapsed-time)

6. **Duration of data usage**
    * **Category**: *Duration*
    * **Purpose**: *Usage limited to a specific time periode*
    * **status**: In Review - [Limitation in the possible usage of data for a specific event](OdrlDirectory.md#3-limitation-in-the-possible-usage-of-data-for-a-specific-event-to-be-determined) and 
    [Limitation in the possible usage of data for a specific time limit](OdrlDirectory.md#4-limitation-in-the-possible-usage-of-data-up-to-a-time-limit)

7. **Activity area**
    * **Category**: *Activity area / sector*
    * **Purpose**: *No restriction (all sectors allowed)*
    * **status**: In Review - [No-restriction over a specific sector](OdrlDirectory.md#1-no-restriction-over-a-specific-sector)

8. **Activity area**
    * **Category**: *Activity area / sector*
    * **Purpose**: *Usage restricted to a list of sectors*
    * **status**: In Review - [Limitation in the possible usage of data for a specific sector](OdrlDirectory.md#2-limitation-in-the-possible-usage-of-data-for-a-specific-sector)
    
9. **Activity area**
    * **Category**: *Activity area / sector*
    * **Purpose**: *Usage allowed in all aeras but a list of sectors*
    * **status**: In Review - [Limitation in the possible usage of data excluding a list of specific sectors](OdrlDirectory.md#3-limitation-in-the-possible-usage-of-data-excluding-a-list-of-specific-sectors)

10. **Exclusif usage**
    * **Category**: *exclusivity*
    * **Purpose**: *No exclusivity*
    * **status**: Todo

11. **Exclusif usage**
    * **Category**: *exclusivity*
    * **Purpose**: *usage exclusif to a specific data consumer*
    * **status**: Todo

12. **Scope of the right of use**
    * **Category**: *Sublicensing*
    * **Purpose**: *Right to sublicense Right (no right, for subsidiaries only, for a specific list of companies, with or without the right to sublicense with or without prior authorization.)*
    * **status**: Todo

13.  **Setting the pricing**
        * **Category**: *Pricing*
        * **Purpose**: *Setting the price of the product and also define the currency and the price units (price unit, price currency, price amount)*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

14.  **Service level availability**
        * **Category**: *Service Levels*
        * **Purpose**: *description: Set the availability of the service \
        The server is available during support hours\
        percentage: 99.9% \
        or (availability": "0.99999","responsetime": "1s")*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

15.  **Service level retention**
        * **Category**: *Service Levels*
        * **Purpose**: *description: Set the retention of data \
        Data is retained for one year\
        period:P1Y
        unlimited: false*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

16.  **Service level latency**
        * **Category**: *Service Levels*
        * **Purpose**: *description: Data is available within 25 hours after the order was placed\
        ex: threshold: 25h\
        sourceTimestampField: orders.order_timestamp\
    processedTimestampField: orders.processed_timestamp*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

17.  **Service level freshness**
        * **Category**: *Service Levels*
        * **Purpose**: *description: description: The age of the youngest row in a table.\
    threshold: 25h\
    timestampField: orders.order_timestamp*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

18.  **Service level Frequency**
        * **Category**: *Service Levels*
        * **Purpose**: *description: Data is delivered once a day\
    type: batch # or streaming\
    interval: daily # for batch, either or cron\
    cron: 0 0 * * * # for batch, either or interval*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

19.  **Service level Support**
        * **Category**: *Service Levels*
        * **Purpose**: *description: The data is available during typical business hours at headquarters\
        time: 9am to 5pm in EST on business days\
    responseTime: 1h\
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

20.  **Service level Backup**
        * **Category**: *Service Levels*
        * **Purpose**: *description: Data is backed up once a week, every Sunday at 0:00 UTC.\
        interval: weekly\
    cron: 0 0 * * 0\
    recoveryTime: 24 hours\
    recoveryPoint: 1 week*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

21.  **Data Quality: Schema Checks**
        * **Category**: *Quality*
        * **Purpose**: * Schema checks: All data types should be followed. \
        Schema 	https://example.com/checkout/webshop-orders/bigquery_orders_latest_npii_v1/schema.yaml*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

22.  **Data Volumes**
        * **Category**: *Consumption*
        * **Purpose**: * Data volume: 5,000-10,000\
        orders per day expected, ~50 KiB / order
Terms\
Max queries per minute: 10\
Max data processing per day: 1 TiB \
Number of Pub/Sub subscriptions: 3*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

23.  **Data Roles: specify the responsability over data**
        * **Category**: *Data Governance*
        * **Purpose**: *In the data contract, data governance establishes guidelines for managing data responsibly. It clarifies roles, responsibilities, and accountability, ensuring compliance with regulations and fostering trust among stakeholders. \
        Roles: (role, access, approvers)\
        Type of access: Read / Copy / Transfert*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

24.  **Data Storage: Source of the data, destination of the data**
        * **Category**: *Data Governance*
        * **Purpose**: *Define constraint over the Source of the data or the destination of the data*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

25.  **Time limit to access a data product**
        * **Category**: *Data Governance*
        * **Purpose**: *Define in which timeframe the data could be accessed*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

26.  **Restriction over types of data**
        * **Category**: *Data Governance*
        * **Purpose**: *Names of the columns with restricted access or visibility \
         Names of columns with sensitive information \
        How sensitive data is represented within the dataset
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

27.  **Observability: Constrains over the event to be obeserved**
        * **Category**: *Observability*
        * **Purpose**: *Define constraint over the event and the destination of observations\
        List of events\
        Identity of the observer*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

28.  **Certification or Validation**
        * **Category**: *Quality*
        * **Purpose**: *Define constraint over the Certification or Validation \
        List of certifications. Identity of the verifiers*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

29.  **Delegation of Access**
        * **Category**: *Access*
        * **Purpose**: *Define who and how to provide access to the data. Ex: a company delegating access to its employees, providing a link to the policy endpoint or DID document to be checked in order to provide access*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

30.  **Deletion and Retention**
        * **Category**: *Data Governance*
        * **Purpose**: *Conditions for Reporting Results and Public Release of Data*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do

31.  **Penalties and resolution of conflicts**
        * **Category**: *Data Governance*
        * **Purpose**: *Penalties for Unauthorized Disclosure of Information, Definition of a Breach, Resolution of Conflicts*
        * **Full wording in is legal form (+source)**: *description*
        * **Status**: to do



