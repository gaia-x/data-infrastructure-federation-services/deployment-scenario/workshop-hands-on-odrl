# ODRL Contract Directory

[TOC]

## Location 

### 1. Specific location restriction

* **Purpose**: *restrict the validity of a permission to a specific location*
* **Full wording in is legal form**: Photographs are often licensed on a geographic basis: global rights will likely be more expensive than a country-specific permission. Such conditions are frequently modelled in ODRL by Constraints. The way of representing a geographic entity has not been standardized, though. Sample from https://w3c.github.io/odrl/bp/#how-to-restrict-the-validity-of-a-permission-to-a-specific-location

```json
    {
        "@context": "http://www.w3.org/ns/odrl.jsonld",
        "@type": "Set",
        "uid": "https://w3c.github.io/odrl/bp/examples/3",
        "permission": [{
            "target": "http://example.com/asset:9898.movie",
            "action": "display",
            "constraint": [{
                "leftOperand": "spatial",
                "operator": "eq",
                "rightOperand":  "https://www.wikidata.org/resource/Q183",
                    "dct:comment": "i.e Germany"
            }]
        }]
    }

```

## Purpose 

### Description of the list of purposes
List of the purposes from the Data Privacy Vocabulary: https://w3c.github.io/dpv/dpv/#vocab-purposes

### 1. No-restriction in the possible usage of data for a specific purpose

* **Purpose**: *No restrictions*
* **Full wording in is legal form**: 

```json
    No specific odrl constraint are needed
```


### 2. Limitation in the possible usage of data for a specific purpose

* **Purpose**: *restrict the validity of a permission to a specific usage : for only non-commercial usage*
* **Full wording in is legal form**: 

```json
    
    {
    "@context": "http://www.w3.org/ns/odrl.jsonld",
    "uid": "http://example.org/policy/1234",
    "type": "Set",
    "permission": [
        {
        "target": "http://example.org/content/article1234",
        "action": "use",
        "constraint": {
            "leftOperand": "purpose",
            "operator": "eq",
            "rightOperand": "non-commercial"
        }
        },
        {
        "target": "http://example.org/content/article1234",
        "action": "distribute",
        "constraint": {
            "leftOperand": "purpose",
            "operator": "eq",
            "rightOperand": "non-commercial"
        }
        }
    ],
    "prohibition": [
        {
        "target": "http://example.org/content/article1234",
        "action": "modify"
        }
    ]
    }
    
```

### 3. Limitation in the possible usage of data for a list of specific purpose

* **Purpose**: *restrict the validity of a permission to a specific usage : for only non-commercial usage*
* **Full wording in is legal form**: 

```json
{
    "@context": "http://www.w3.org/ns/odrl.jsonld",
    "uid": "http://example.org/policy/1234",
    "type": "Set",
    "permission": [
        {
        "target": "http://example.org/content/article1234",
        "action": "use",
        "constraint": {
            "leftOperand": "purpose",
            "operator": "isNoneOf",
            "rightOperand": ["non-commercial"]
        }
        },
        {
        "target": "http://example.org/content/article1234",
        "action": "distribute",
        "constraint": {
            "leftOperand": "purpose",
            "operator": "isNoneOf",
            "rightOperand": ["non-commercial"]
        }
        }
    ],
    "prohibition": [
        {
        "target": "http://example.org/content/article1234",
        "action": "modify"
        }
    ]
    }
```

### 4. Academic Research Purpose restriction with mandatory attribution

* **Purpose**: *Below is an example of an ODRL policy that restricts the use of data specifically to academic research purposes. This policy allows data to be used only for academic research, within a specific geographic region, and for a limited period. Additionally, it imposes a duty to attribute the source when the data is used.*
* **Full wording in is legal form**: 

```json
    
{
  "@context": "http://www.w3.org/ns/odrl.jsonld",
  "uid": "http://example.org/policy/5678",
  "type": "Set",
  "permission": [
    {
      "target": "http://example.org/data/dataset1234",
      "action": "use",
      "constraint": [
        {
          "leftOperand": "purpose",
          "operator": "eq",
          "rightOperand": "academic-research"
        },
        {
          "leftOperand": "spatial",
          "operator": "eq",
          "rightOperand": "http://example.org/geo/restricted-area"
        },
        {
          "leftOperand": "elapsedTime",
          "operator": "lt",
          "rightOperand": "P1Y"  // One year period
        }
      ],
      "duty": [
        {
          "action": "attribution",
          "target": "http://example.org/data/dataset1234",
          "assignee": "http://example.org/party/data-user",
          "constraint": {
            "leftOperand": "attributionNotice",
            "operator": "isA",
            "rightOperand": "required"
          }
        }
      ]
    }
  ],
  "prohibition": [
    {
      "target": "http://example.org/data/dataset1234",
      "action": "modify"
    },
    {
      "target": "http://example.org/data/dataset1234",
      "action": "commercialize"
    }
  ]
}
    
```


The term "academic-research" is not part of the core ODRL vocabulary and would need to be defined within a specific namespace or extended vocabulary that is recognized by the community using the ODRL policies.

To properly define "academic-research" as a purpose constraint, you can create a custom vocabulary or use an existing extended vocabulary that includes academic research terms. Here's an example using a custom namespace for academic research purposes:

```json
{
  "@context": [
    "http://www.w3.org/ns/odrl.jsonld",
    {
      "academic-research": "http://example.org/vocab/purpose#academic-research"
    }
  ],
  "uid": "http://example.org/policy/5678",
  "type": "Set",
  "permission": [
    {
      "target": "http://example.org/data/dataset1234",
      "action": "use",
      "constraint": [
        {
          "leftOperand": "purpose",
          "operator": "eq",
          "rightOperand": "academic-research"
        },
        {
          "leftOperand": "spatial",
          "operator": "eq",
          "rightOperand": "http://example.org/geo/restricted-area"
        },
        {
          "leftOperand": "elapsedTime",
          "operator": "lt",
          "rightOperand": "P1Y"  // One year period
        }
      ],
      "duty": [
        {
          "action": "attribution",
          "target": "http://example.org/data/dataset1234",
          "assignee": "http://example.org/party/data-user",
          "constraint": {
            "leftOperand": "attributionNotice",
            "operator": "isA",
            "rightOperand": "required"
          }
        }
      ]
    }
  ],
  "prohibition": [
    {
      "target": "http://example.org/data/dataset1234",
      "action": "modify"
    },
    {
      "target": "http://example.org/data/dataset1234",
      "action": "commercialize"
    }
  ]
}
```
The term "commercialize" is not defined in the core ODRL vocabulary. The core ODRL vocabulary primarily includes general terms related to permissions, prohibitions, and actions like "use," "distribute," "modify," and similar actions.

To use a term like "commercialize," you would need to define it in a custom vocabulary or use an existing extended vocabulary that includes terms specific to commercialization. Here’s an example of how you can create a custom namespace for such a term:
```json
{
  "@context": [
    "http://www.w3.org/ns/odrl.jsonld",
    {
      "custom": "http://example.org/vocab/actions#"
    }
  ],
  "uid": "http://example.org/policy/5678",
  "type": "Set",
  "permission": [
    {
      "target": "http://example.org/data/dataset1234",
      "action": "use",
      "constraint": [
        {
          "leftOperand": "purpose",
          "operator": "eq",
          "rightOperand": "http://example.org/vocab/purpose#academic-research"
        },
        {
          "leftOperand": "spatial",
          "operator": "eq",
          "rightOperand": "http://example.org/geo/restricted-area"
        },
        {
          "leftOperand": "elapsedTime",
          "operator": "lt",
          "rightOperand": "P1Y"  // One year period
        }
      ],
      "duty": [
        {
          "action": "attribution",
          "target": "http://example.org/data/dataset1234",
          "assignee": "http://example.org/party/data-user",
          "constraint": {
            "leftOperand": "attributionNotice",
            "operator": "isA",
            "rightOperand": "required"
          }
        }
      ]
    }
  ],
  "prohibition": [
    {
      "target": "http://example.org/data/dataset1234",
      "action": "modify"
    },
    {
      "target": "http://example.org/data/dataset1234",
      "action": "custom:commercialize"
    }
  ]
}
```
The Data Privacy Vocabulary (DPV) https://w3c.github.io/dpv/dpv can be integrated into an ODRL policy to enhance the expression of data privacy-related terms. Here’s an example of how to incorporate the DPV into an ODRL policy that restricts data usage to academic research purposes.

```json
{
  "@context": [
    "http://www.w3.org/ns/odrl.jsonld",
    {
      "dpv": "http://www.w3.org/ns/dpv#"
    }
  ],
  "uid": "http://example.org/policy/5678",
  "type": "Set",
  "permission": [
    {
      "target": "http://example.org/data/dataset1234",
      "action": "use",
      "constraint": [
        {
          "leftOperand": "http://www.w3.org/ns/odrl/2/purpose",
          "operator": "eq",
          "rightOperand": "dpv:AcademicResearch"
        },
        {
          "leftOperand": "http://www.w3.org/ns/odrl/2/spatial",
          "operator": "eq",
          "rightOperand": "http://example.org/geo/restricted-area"
        },
        {
          "leftOperand": "http://www.w3.org/ns/odrl/2/elapsedTime",
          "operator": "lt",
          "rightOperand": "P1Y"  // One year period
        }
      ],
      "duty": [
        {
          "action": "http://www.w3.org/ns/odrl/2/attribution",
          "target": "http://example.org/data/dataset1234"
        }
      ]
    }
  ],
  "prohibition": [
    {
      "target": "http://example.org/data/dataset1234",
      "action": "http://www.w3.org/ns/odrl/2/modify"
    },
    {
      "target": "http://example.org/data/dataset1234",
      "action": "http://www.w3.org/ns/odrl/2/CommercialUse"
    }
  ]
}


```
or 

```json
{
  "@context": [
    "http://www.w3.org/ns/odrl.jsonld",
    {
      "dpv": "http://www.w3.org/ns/dpv#"
    }
  ],
  "uid": "http://example.org/policy/5678",
  "type": "Set",
  "permission": [
    {
      "target": "http://example.org/data/dataset1234",
      "action": "use",
      "constraint": [
        {
          "leftOperand": "purpose",
          "operator": "eq",
          "rightOperand": "dpv:AcademicResearch"
        },
        {
          "leftOperand": "spatial",
          "operator": "eq",
          "rightOperand": "http://example.org/geo/restricted-area"
        },
        {
          "leftOperand": "elapsedTime",
          "operator": "lt",
          "rightOperand": "P1Y"  // One year period
        }
      ],
      "duty": [
        {
          "action": "attribution",
          "target": "http://example.org/data/dataset1234"
        }
      ]
    }
  ],
  "prohibition": [
    {
      "target": "http://example.org/data/dataset1234",
      "action": "modify"
    },
    {
      "target": "http://example.org/data/dataset1234",
      "action": "CommercialUse"
    }
  ]
}
```


## Duration 

Odrl offers a full set of description of time: Elapsed Time or Timedate

### 1. No-restriction in the possible usage for a specific periode of time

* **Purpose**: *No restrictions*
* **Full wording in is legal form**: 

```json
    No specific odrl constraint are needed
```


### 2. Limitation in the possible usage of data for a specific periode of time (Elapsed Time)

* **Purpose**: *restrict the validity of a permission to a specific periode of time : On Year Period*
* **Full wording in is legal form**: 

```json
    
    {
  "@context": [
    "http://www.w3.org/ns/odrl.jsonld",
    {
      "academic-research": "http://example.org/vocab/purpose#academic-research"
    }
  ],
  "uid": "http://example.org/policy/5678",
  "type": "Set",
  "permission": [
    {
      "target": "http://example.org/data/dataset1234",
      "action": "use",
      "constraint": [
        {
          "leftOperand": "elapsedTime",
          "operator": "lt",
          "rightOperand": "P1Y"  // One year period
        }
      ]
    }
  ]
}
```

### 3. Limitation in the possible usage of data for a specific Event (to be determined)

* **Purpose**: *restrict the validity of a permission to a specific periode of time : A defined Event*
* **Full wording in is legal form**: 

```json
{
  "@context": [
    "http://www.w3.org/ns/odrl.jsonld",
    {
      "academic-research": "http://example.org/vocab/purpose#academic-research"
    }
  ],
  "uid": "http://example.org/policy/5678",
  "type": "Set",
  "permission": [
    {
      "target": "http://example.org/data/dataset1234",
      "action": "use",
      "constraint": [
        {
          "leftOperand": "Event",
          "operator": "eq",
          "rightOperand": "Gaia-X Tech-X 2024"  // 
        }
      ]
    }
  ]
}
```

### 4. Limitation in the possible usage of data up to a time limit

* **Purpose**: *restrict the validity of a permission up to a time limit (Define in which timeframe the data could be accessed)*
* **Full wording in is legal form**: 

```json
{
  "@context": [
    "http://www.w3.org/ns/odrl.jsonld",
    {
      "academic-research": "http://example.org/vocab/purpose#academic-research"
    }
  ],
  "uid": "http://example.org/policy/5678",
  "type": "Set",
  "permission": [
    {
      "target": "http://example.org/data/dataset1234",
      "action": "use",
      "constraint": [
        {
          "leftOperand": "Datetime",
          "operator": "gteq",
          "rightOperand": "2025-12-31T23:59Z"  // 
        }
      ]
    }
  ]
}

```



## Enforcing Technical Measure 

Enforcement:
Difference between the access (forbid or not) that need to be check in realtime and the usage (where you check afterwards) to be used in court for instance

### 1. How to enforce Activity Monitoring?

* **Purpose**: *Below is an example of an ODRL policy that enforces *
* **Full wording in is legal form**: 

```json
    
{
    "@context": ["http://www.w3.org/ns/odrl.jsonld",{
      "dpv": "http://www.w3.org/ns/dpv#"
    }

],

(...)

    "duty": [
            {
            "action": "dpv:activityMonitoring",
            "target": "http://example.org/data/dataset1234",
            "assignee": "http://example.org/party/data-user",
            "constraint": {
                "leftOperand": "attributionNotice",
                "operator": "isA",
                "rightOperand": "required"
            }
            }
        ]
}
```


## Activity Area / Sectors

### Description of the list of sectors

Vocabulary that could be used to define industries: https://metadata.ilo.org/taxonomy.html
List of industry sectors: https://metadata.ilo.org/taxonomy/985.html

### 1. No-restriction over a specific sector

* **Purpose**: *No restrictions (all sectors are allowed*
* **Full wording in is legal form**: 

```json
    No specific odrl constraint are needed
```


### 2. Limitation in the possible usage of data for a specific sector

* **Purpose**: *restrict the validity of a permission to a specific sector : Clothing & textile industries*
* **Full wording in is legal form**: 

```json
    
   {
  "@context": [
    "http://www.w3.org/ns/odrl.jsonld",
    {
      
    }
  ],
  "uid": "http://example.org/policy/5678",
  "type": "Set",
  "permission": [
    {
      "target": "http://example.org/data/dataset1234",
      "action": "use",
      "constraint": [
        {
          "leftOperand": "Industry",
          "operator": "eq",
          "rightOperand": "Clothing and textile industries"  // 
        }
      ]
    }
  ]
}
    
```

### 3. Limitation in the possible usage of data excluding a list of specific sectors

* **Purpose**: *permission is granted to all sectors except for some specific sectors*
* **Full wording in is legal form**: 

```json
{
  "@context": [
    "http://www.w3.org/ns/odrl.jsonld",
    {
      
    }
  ],
  "uid": "http://example.org/policy/5678",
  "type": "Set",
  "permission": [
    {
      "target": "http://example.org/data/dataset1234",
      "action": "use",
      "constraint": [
        {
          "leftOperand": "Industry",
          "operator": "isNoneOf",
          "rightOperand": ["Clothing and textile industries"]  
        }
      ]
    }
  ]
}

```