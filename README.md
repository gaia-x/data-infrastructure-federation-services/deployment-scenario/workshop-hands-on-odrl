[TOC]

# Workshop - Hands on odrl

A repository hosting the file for the Workshop "Hands-On-ODRL"

**Date**: Friday May 24th, 2024 

**Location**: the Gaia-X Tech-X hackathon room


# Goal 
Translating and expressing contractual clause in ODRL format in order to:
* provide a book of contractual clause ready-be-used
* identify terms to be inserted in a Gaia-X ODRL Profile

# Repository Map
🙋 - I need help on a clause 👉 describe your contractual clause to be translated here [Contract Translation Requests List](ContractSample.md)

🤴 - I'm an ODRL King and I want to help translating contractual clause into ODRL 👉 Join a group, ge assign clauses, and work on it

!! important !! before you start writing custom vocabularies, make sure that there is not already an existing one in the [Existing Vocabulary](UsingExistingVocabulariesforODRL.md) page

(***Soonly after the workshop***) 🕵 - You search how to translate a constrain in ODRL. 👉 Check here in the directory for the compiled result [ORDL Contract Clause Directory](OdrlDirectory.md)

# How-to

1. Provide contract sample in the list of samples
2. After a short presentation of ORDL principles, people are divided into groups
3. Each groupes is assigned a list of sample to transform
4. All ODRL translation are pushed in the repository

# Samples
* https://w3c.github.io/odrl/bp/#styles
* https://faircookbook.elixir-europe.org/content/recipes/reusability/expressing-data-use.html#encoding-research-restriction-on-disease-and-geographical-area-using-odrl-and-duo
* Data Licences: https://www.dalicc.net/license-library/ and to get the ODRL part: https://api.dalicc.net/docs#/licenselibrary/get_license_by_id_licenselibrary_license__license_id__get

# Tools & References

## References documents:
* ODRL specs: https://www.w3.org/TR/odrl-model/
* ODRL Core vocabulary: https://www.w3.org/TR/odrl-vocab/#vocab-core
* ODRL Common vocabulary: https://www.w3.org/TR/odrl-vocab/#vocab-common
* ODRL Profiles: https://www.w3.org/TR/odrl-model/#profile

## Tools
* policy decision point (Wizard LAB): https://wizard.lab.gaia-x.eu/policyStepper
